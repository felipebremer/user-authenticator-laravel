php artisan make:scaffold City ^
 --schema="name:string, abbreviation:string, friendly_url:string" ^
 --validator="name:required, abbreviation:required, friendly_url:required"